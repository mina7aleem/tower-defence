﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace TurretBehaviour
    {
        public class TurretBulletBehaviour : MonoBehaviour
        {
            private int moveSpeed = 10;
            float fireRange = 2f;
            void Start()
            {
                
            }

            void OnEnable() 
            {
                GameObject[] cars = GameObject.FindGameObjectsWithTag("Car");
                foreach (GameObject car in cars)
                {
                    if (Vector3.Distance(transform.position, car.transform.position) <= fireRange)
                    {
                        transform.LookAt(car.transform);
                    }
                }
                StartCoroutine("WaitForDeactivate");
            }

            void Update()
            {
                MoveDirection();
            }

            void MoveDirection()
            {
                transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            }

            IEnumerator WaitForDeactivate()
            {
                yield return new WaitForSeconds(0.8f);
                gameObject.SetActive(false);
            }
        }
    }
}


