﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameSystem
{
    namespace TurretBehaviour
    {
        public class TurretBehaviour : MonoBehaviour
        {
            [SerializeField] InstantiationSystem.InstantiationManager instantiationManager;
            [SerializeField] Transform turretFirePoint;
            float timer = 0f;
            float fireRange = 2f;
            void Start()
            {

            }

            void Update()
            {
                DetectDestroyCar();
            }
            void DetectDestroyCar()
            {
                GameObject[] cars = GameObject.FindGameObjectsWithTag("Car");
                foreach (GameObject car in cars)
                {
                    if (Vector3.Distance(transform.position, car.transform.position) <= fireRange)
                    {
                        transform.LookAt(car.transform);
                        TurretFire();
                    }
                }
            }

            void TurretFire()
            {
                timer += Time.deltaTime;
                if(timer > 0.5f)
                {
                    GameObject turretBullet = instantiationManager.GetPooledTurretBullet();
                    turretBullet.transform.position = turretFirePoint.position;
                    turretBullet.SetActive(true);
                    timer = 0;
                }
            }
        }
    }
}


