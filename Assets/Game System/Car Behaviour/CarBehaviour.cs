﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace CarBehaviour
    {
        public class CarBehaviour : MonoBehaviour
        {
            private GameObject[] pathPoints;
            private float moveSpeed = 1;
            [SerializeField] int i = 0;
            void Start()
            {
                pathPoints = GameObject.FindGameObjectsWithTag("PathPoint");
            }

            void OnEnable() 
            {
                i = 0;
            }

            void Update()
            {
                CarMotion();
            }

            void CarMotion()
            {
                Vector3 moveDir = pathPoints[i].transform.position - transform.position;
                transform.LookAt(pathPoints[i].transform);
                transform.Translate(moveDir.normalized * moveSpeed * Time.deltaTime, Space.World);
                if (Vector3.Distance(transform.position, pathPoints[i].transform.position) <= 0.02f)
                {
                    if (i < pathPoints.Length-1)
                    {
                        i++;
                    }
                    else
                    {
                        //Destroy(gameObject);
                        gameObject.SetActive(false);                        
                    }
                }
            }
        }
    }
}


