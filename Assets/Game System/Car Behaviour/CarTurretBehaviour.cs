﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameSystem
{
    namespace CarBehaviour
    {
        public class CarTurretBehaviour : MonoBehaviour
        {
            GameObject[] turrets;
            void Start()
            {
                turrets = GameObject.FindGameObjectsWithTag("Turret");
            }

            void Update()
            {
                Rotate();
            }

            void Rotate()
            {
                for (int i = 0; i < turrets.Length; i++)
                {
                    if (Vector3.Distance(transform.position, turrets[i].transform.position) <= 2f)
                    {
                        transform.LookAt(turrets[i].transform); 
                    }
                }
                
                
            }
        }
    }
}


