﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameSystem
{
    namespace CarBehaviour
    {
        public class CarHealth : MonoBehaviour
        {
            [SerializeField] Image healthBarImage;
            GameObject explosion;
            void Awake()
            {
                explosion = gameObject.transform.Find("Explosion").gameObject;
            }

            void OnEnable() 
            {
                healthBarImage.fillAmount = 1;
                if (explosion.activeInHierarchy)
                {
                    explosion.SetActive(false);
                }
                
            }

            void Update()
            {
                
            }
            void OnTriggerEnter(Collider other) 
            {
                if (other.tag == "TurretBullet")
                {   
                    if (healthBarImage.fillAmount > 0)
                    {
                        healthBarImage.fillAmount -=0.1f;
                        if (healthBarImage.fillAmount == 0)
                        {
                            explosion.SetActive(true);
                            StartCoroutine(CarDeactivation());
                        }
                    }
                }
            }

            IEnumerator CarDeactivation()
            {
                yield return new WaitForSeconds(0.5f);
                gameObject.SetActive(false);
            }
        }
    }
}


