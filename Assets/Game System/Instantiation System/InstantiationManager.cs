using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace InstantiationSystem
    {
        public class InstantiationManager : MonoBehaviour
        {
            [Header("Turret Bullet")]
            [SerializeField] GameObject turretBullet;
            private List<GameObject> turretBulletPool = new List<GameObject>();
            private int turretBulletPoolSize = 20;
            [Header("Car")]
            [SerializeField] GameObject car;
            private List<GameObject> carPool = new List<GameObject>();
            private int carPoolSize = 20;
            [SerializeField] Transform carInstPoint;
            float timer = 0f;
            void Start()
            {
                FillingCarPool();
                FillingTurretBulletPool();
            }
            void Update()
            {
                SpawnCar();
            }
            //----------TurretBullet------------------------------------
            void FillingTurretBulletPool()
            {
                for (int i = 0; i < turretBulletPoolSize; i++)
                {
                    GameObject spawnedTurretBullet = Instantiate(turretBullet);
                    spawnedTurretBullet.SetActive(false);
                    turretBulletPool.Add(spawnedTurretBullet);
                }
            }
            public GameObject GetPooledTurretBullet()
            {
                for (int i = 0; i < turretBulletPool.Count; i++)
                {
                    if (!turretBulletPool[i].activeInHierarchy)
                    {
                        return turretBulletPool[i];
                    }
                }
                return null;
            }
            //-----------Car------------------------------------
            void FillingCarPool()
            {
                for (int i = 0; i < carPoolSize; i++)
                {
                    GameObject spawnedCar = Instantiate(car);
                    spawnedCar.SetActive(false);
                    carPool.Add(spawnedCar);
                }
            }
            public GameObject GetPooledCar()
            {
                for (int i = 0; i < carPool.Count; i++)
                {
                    if (!carPool[i].activeInHierarchy)
                    {
                        return carPool[i];
                    }
                }
                return null;
            }
            void SpawnCar()
            {
                 timer += Time.deltaTime;
                if(timer > 5)
                {
                    GameObject spawnedCar = GetPooledCar();
                    spawnedCar.transform.position = carInstPoint.position;
                    spawnedCar.SetActive(true);
                    timer = 0f;
                } 
            }
        }
    }
}


